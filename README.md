### Workshop 2016 AngularJS


#### Requerimientos previos


- Computadora con Linux o Windows
- Git (si estás en Windows usá la consola de Git bash)
- Node.js 6.0 o superior
- npm 3.0 o superior


#### Instrucciones de uso


- 1 Instalar las dependencias


```
   npm install
```


- 2 Iniciar el server de dev


```
    npm run dev
```


#### Descripción del proyecto
Con el desarrollo de esta app vas a aprender cómo comunicarte con una api y mostrar esos resultados en diferentes rutas y con componentes acordes a cada tipo de información


#### Esquema de carpetas


```
./
├── package.json        
├── ...
└── /src
    ├── /services
    │   └── api.ts                  # Llamadas a los correspondientes endpoints para conseguir data
    ├── /components
    │   ├── /component1             # carpeta con el codigo, html y css de cada componente
    │   └── /component2
    ├── /pages          
    │    ├── /page-component1       # codigo de los page-components (+ llamada a los servicios)
    │    └── /page-component2
    ├── app.component.ts            # el root component
    ├── app.module.ts               # el modulo del root component
    └── app.routes.ts               # las rutas que apuntan a cada page-component
```


##### API: Como llamar a endpoints


Usamos la clase ApiService que contiene:


- url: path del server a usar
- Métodos que retornan respuestas de cada endpoint


Cada llamada usa una estructura similar a esta:

```javascript
getSomething (...params): Promise<any> {
    return this.http.get(`${this.url}/endpoint/${parameter1}`)
        .toPromise()
        .then(response => response.json());
}
```


##### Router


Estructura de rutas


```javascript
[
    ...
    {
        path: 'path/:pathParameter',
        component: PageComponent,
    }
    ...
]
```


El PageComponent es el bloque responsable por definir la estructura de la pagina (con sus componentes), buscar la data en los servicios y pasarla a los child components.


```javascript
@Component({
    selector: 'page-component',
    template: `
        <h1>My Page</h1>
        <child-component
            [data]="data | async">
        </child-component>
    `,
})
export class PageComponent {
    ...
    ngOnInit() {
        this.data = this.service.getData();
    }
}
```


Asumiendo que data es un array con la siguiente estructura:

```json
[
    {"name": "Mike"},
    {"name": "Eleven"},
    {"name": "Dustin"},
    {"name": "Lucas"},
    {"name": "Will"}
]
```


```
    <h2>Stranger Things Characters:</h2>
    <ul>
        <li *ngFor="character of characters">
            {{character.name}}
        </li>
    </ul>
```


### Have fun!
