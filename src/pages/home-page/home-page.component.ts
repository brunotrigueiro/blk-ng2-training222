import {Component} from '@angular/core';



@Component({
    selector: 'home-page',
    template: `
        <table-list type="band" filter
            [paginate]="5"
            [columns]="[
                {key: 'name'},
                {key: 'genres'},
                {key: 'popularity'}
            ]"
            [data]="data | async">
        </table-list>
        <!-- <pre>{{data | json}}</pre> -->
    `,
})
export class HomePageComponent {}
