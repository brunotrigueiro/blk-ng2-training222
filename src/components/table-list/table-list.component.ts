import {Component, Input} from '@angular/core';

import {TableColumns} from './table-columns.interface';



@Component({
  selector: 'table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss']
})
export class TableListComponent {
    @Input()
    columns: Array<TableColumns> = [];

    @Input()
    data: Array<object>;


    @Input()
    paginate: boolean = false;

    @Input()
    type: string;

}
