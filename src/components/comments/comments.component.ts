import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiService} from '../../services';



@Component({
    selector: 'comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss']

})
export class CommentsComponent {
    @Input() data: object;

    commentField: string;
    trackId: string;


    constructor (
        private service: ApiService,
        private route: ActivatedRoute,
    ) {}


    onSubmit () {
        this.service.postCommentForTrack({
            message: this.commentField,
            name: 'placeholder',
            trackId: this.route.snapshot.params['trackId'],
        })
        .then(response => {
            //   this.route.reload();
            window.location.reload();
            return response.data;
        });
    }

}
