import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, RouterOutlet} from '@angular/router';

import {ApiService} from './services';
import {
    TableListComponent, VerticalListComponent, PaginationComponent,
    CommentsComponent,
} from './components';

import {
    HomePageComponent, BandPageComponent, AlbumPageComponent,
} from './pages';

import {AppComponent} from './app.component';

import {appRoutes} from './app.routes';




@NgModule({
    declarations: [
        TableListComponent,
        VerticalListComponent,
        PaginationComponent,
        CommentsComponent,

        HomePageComponent,
        BandPageComponent,
        AlbumPageComponent,

        AppComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot(appRoutes),
    ],
    providers: [ApiService],
    bootstrap: [AppComponent]
})
export class AppModule { }
